import { EMPTY_RESULT }     from '../parser-combinators/constants.mjs';
import {
    concatResults,
    oneOf,
    oneOrMore,
    sequence,
    withResults,
    wrapResults,
}                           from '../parser-combinators/combinators.mjs';
import {
    preventRecursion,
}                           from '../parser-combinators/context.mjs';

import {
    T_ADDITION,
    T_DIGIT,
    T_DIVISION,
    T_INTEGER,
    T_LITERAL,
    T_MULTIPLICATION,
    T_NATURAL,
    T_PARENTHESIS,
    T_SUBTRACTION,
}                           from './constants.mjs';


/// Terminals ------------------------------------------------------------------

const digit = ( str, pos ) =>
    str[pos]?.match( /[0-9]/ )
        ? [{
            nextPos:        pos + 1,
            pos,
            type:           T_DIGIT,
            value:          str[pos],
        }]
        : EMPTY_RESULT;


const literal = value =>
    ( str, pos ) =>
        value === str.slice( pos, pos + value.length ) 
            ? [{
                nextPos:    pos + value.length,
                pos,
                type:       T_LITERAL,
                value,
            }]
            : EMPTY_RESULT;


/// Values ---------------------------------------------------------------------

const naturalNumber =
    concatResults( T_NATURAL,
        oneOrMore( digit ),
    );


const integer =
    concatResults( T_INTEGER,
        oneOf(
            sequence(
                literal( '-' ),
                naturalNumber,
            ),
            naturalNumber,
        )
    );


/// Operations -----------------------------------------------------------------

const additionOperand = oneOf(
    multiplication,
    division,
    addition,
    subtraction,
    parenthesis,
    integer,
);

const divisionOperand = oneOf(
    multiplication,
    division,
    parenthesis,
    integer,
);

const multiplicationOperand = oneOf(
    multiplication,
    division,
    parenthesis,
    integer,
);

const subtractionOperand = oneOf(
    multiplication,
    division,
    addition,
    subtraction,
    parenthesis,
    integer,
);

const operation = ( type, opString, operand ) =>
    preventRecursion( type,
        wrapResults( type,
            sequence( operand, literal( opString ), operand ),
        ),
    );

function addition( ...args ){
    return operation( T_ADDITION, '+', additionOperand )( ...args );
}

function division( ...args ){
    return operation( T_DIVISION, '/', divisionOperand )( ...args );
}

function multiplication( ...args ){
    return operation( T_MULTIPLICATION, '*', multiplicationOperand )( ...args );
}

function subtraction( ...args ){
    return operation( T_SUBTRACTION, '-', subtractionOperand )( ...args );
}


/// Expressions ----------------------------------------------------------------

function parenthesis( ...args ){
    return wrapResults( T_PARENTHESIS,
        sequence(
            literal( '(' ),
            expression,
            literal( ')' ),
        ),
    )( ...args );
}


function expression( ...args ){
    return oneOf(
        addition,
        subtraction,
        multiplication,
        division,
        parenthesis,
        integer,
    )( ...args );
}

export default expression;

import {
    T_ADDITION,
    T_DIVISION,
    T_INTEGER,
    T_MULTIPLICATION,
    T_PARENTHESIS,
    T_SUBTRACTION,
}                           from './constants.mjs';


const PROCESSORS = {
    [T_ADDITION]: node =>
        processNode( node.children[0] )
        + processNode( node.children[2] ),
    [T_DIVISION]: node =>
        processNode( node.children[0] )
        / processNode( node.children[2] ),
    [T_INTEGER]: node =>
        parseInt( node.value, 10 ),
    [T_MULTIPLICATION]: node =>
        processNode( node.children[0] )
        * processNode( node.children[2] ),
    [T_PARENTHESIS]: node =>
        processNode( node.children[1] ),
    [T_SUBTRACTION]: node =>
        processNode( node.children[0] )
        - processNode( node.children[2] ),
};


function processNode( node ){
    return PROCESSORS[node.type]( node );
}


export default ast =>
    ast.map( processNode );


import { basename }         from 'node:path';

import {
    execute,
    parser,
}                           from './arithmetic/index.mjs';
import parse                from './parser-combinators/index.mjs';


const [ node, script, input ] = process.argv;

if( ! input ){
    console.log( `Usage: ${ basename( node )} ${ basename( script )} INPUT_CODE` );
    process.exit( 1 );
}
console.debug( input );

const ast =                 parse( parser, input );
console.debug( JSON.stringify( ast, null, 4 ));

const output =              execute( ast )[0];
console.log( output );

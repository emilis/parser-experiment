import { createContext }    from './context.mjs';


export default ( parser, str ) => {

    if( ! str.length ){
        throw Error( 'Empty input string.' );
    }

    const results =         parser( str, 0, createContext() );
    if( ! results.length ){
        throw Error( `Failed to parse input string.` );
    }

    const endPos =          results.at( -1 ).nextPos;
    if( endPos !== str.length ){
        console.error( JSON.stringify( results, null, 4 ));
        throw Error( `Unexpected char at ${ endPos }: ${ str.slice( endPos, 16 )}.` );
    }

    return results;
};

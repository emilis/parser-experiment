import { EMPTY_RESULT }     from './constants.mjs';


export const withResults = ( fn, parser ) =>
    ( ...parserArgs ) => {
        const  results =    parser( ...parserArgs );
        return results.length
            ? fn( results )
            : EMPTY_RESULT;
    };

export const concatResults = ( type, parser ) =>
    withResults(
        results => [{
            nextPos:    results.at( -1 ).nextPos,
            pos:        results[0].pos,
            type,
            value: results
                .map( r => r.value )
                .join( '' ),
        }],
        parser,
    );

export const wrapResults = ( type, parser ) =>
    withResults(
        results => [{
            children:   results,
            nextPos:    results.at( -1 ).nextPos,
            pos:        results[0].pos,
            type,
        }],
        parser,
    );


export const oneOf = ( ...parsers ) =>
    ( ...parserArgs ) => {
        for( const parser of parsers ){
            const result =  parser( ...parserArgs );
            if( result.length ){
                return result;
            }
        }
        return EMPTY_RESULT;
    };


export const oneOrMore = parser =>
    ( str, initialPos, context )=> {
        let pResult =       EMPTY_RESULT;
        let pos =           initialPos;
        let result =        [];
        do {
            pResult =       parser( str, pos, context );
            if( pResult.length ){
                pos =       pResult[0].nextPos;
                result = [
                    ...result,
                    ...pResult,
                ];
            }
        } while( pResult.length );
        return result;
    };


export const sequence = ( ...parsers ) =>
    ( str, initialPos, context ) => {
        let pos =           initialPos;
        let result =        [];
        for( const parser of parsers ){
            const pResult = parser( str, pos, context );
            if( ! pResult.length ){
                return EMPTY_RESULT;
            }
            pos =           pResult[0].nextPos;
            result = [
                ...result,
                ...pResult,
            ];
        }
        return result;
    };

import { EMPTY_RESULT }     from './constants.mjs';


export const preventRecursion = ( key, parser ) =>
    ( str, pos, context ) => {
        const pKey =        `${ key }:${ pos }`;

        if( context.path.includes( pKey )){
            //console.debug( 'preventRecursion', pos, key, 'NOPE' );
            return EMPTY_RESULT;
        } else {
            context.path.push( pKey );
            //console.debug( 'preventRecursion', pos, key, 'TRY', context.path );
            const results =  parser( str, pos, context );
            //console.debug( 'preventRecursion', pos, key, 'OUT', context.path, results);
            context.path.pop();
            return results;
        }
    };


export const createContext = () => ({
    path:   [],
});

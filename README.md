# An experiment implementing parser combinators in JS

## Usage:

```
node main.js 1+2*3
```

## License

Copyright 2023 Emilis Dambauskas dev@emilis.net

This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE.txt for details.
